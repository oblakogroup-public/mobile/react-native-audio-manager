//
//  AudioRecorder.swift
//  AudioManager
//
//  Created by Oleg Efimov on 14.03.2023.
//

import AVFoundation

/// Class to keep mic active
final class AudioRecorder: NSObject {
    
    public private(set) var shouldRecordMic = false
    
    //MARK: Private Props
    
    private var audioRecorder: AVAudioRecorder?
    
    private var isRecordingEnabled: Bool {
        audioRecorder?.isRecording == true
    }
    
    private let debugTag = "AudioManager AudioRecorder: "
    
    private let tempAudioFileURL = FileManager.default
        .temporaryDirectory.appendingPathComponent("recording.wav")
    
    //MARK: Init
    
    override init() {
        super.init()
        
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(routeChangeHandler(notification:)),
            name: AVAudioSession.routeChangeNotification,
            object: nil)
        
        setUpRecording()
    }
    
    deinit {
        invalidateRecordingTask()
        NotificationCenter.default.removeObserver(self)
    }
    
    //MARK: Public Methods
    
    public func startRecording() {
        shouldRecordMic = true
        
        guard !isRecordingEnabled
        else {
            printLogs(log: "Warning: Recorder is already in use")
            return
        }
        
        beginRecording()
    }
    
    public func stopRecording() {
        shouldRecordMic = false
        
        guard isRecordingEnabled
        else { return }
        
        invalidateRecordingTask()
    }
    
    public func restartRecorder() {
        shouldRecordMic = true
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5)
        { [weak self] in
            guard let strongSelf = self,
                  strongSelf.shouldRecordMic
            else { return }
            
            strongSelf.printLogs(log: "Restart recording")
            strongSelf.stopRecording()
            strongSelf.startRecording()
        }
    }
    
    //MARK: Private Methods
    
    @objc private func routeChangeHandler(notification: Notification) {
        if shouldRecordMic {
            restartRecorder()
        }
    }
    
    private func printLogs(log: String) {
        print("\(debugTag) \(log)")
    }
    
    private func invalidateRecordingTask() {
        if isRecordingEnabled {
            audioRecorder?.stop()
            audioRecorder?.deleteRecording()
        }
        
        try? FileManager.default.removeItem(at: tempAudioFileURL.absoluteURL)
    }
    
    private func setUpRecording() {
        let settings = [
            AVFormatIDKey: Int(kAudioFormatLinearPCM),
            AVSampleRateKey: 6000,
            AVNumberOfChannelsKey: 1,
            AVEncoderAudioQualityKey: AVAudioQuality.min.rawValue,
            AVEncoderBitDepthHintKey: 8
        ]
        
        do {
            let recorder = try AVAudioRecorder(url: tempAudioFileURL, settings: settings)
            printLogs(log: "SET RECORDER \(recorder.settings)")
            recorder.delegate = self
            recorder.record()
            recorder.stop()
            recorder.deleteRecording()
            
            self.audioRecorder = recorder
        } catch let error {
            printLogs(log: "RECORD FAILEd \(error.localizedDescription)")
        }
        
    }
    
    private func beginRecording() {
        printLogs(log: "Begin record")
        
        audioRecorder?.record(forDuration: .infinity)
    }
}

//MARK: AVAudioRecorderDelegate

extension AudioRecorder: AVAudioRecorderDelegate {
    func audioRecorderDidFinishRecording(_ recorder: AVAudioRecorder, successfully flag: Bool) {
        printLogs(log: "Finish recording with flag success: \(flag)")
    }
    
    func audioRecorderEncodeErrorDidOccur(_ recorder: AVAudioRecorder, error: Error?) {
        guard let error = error
        else { return }
        
        printLogs(log: "Error: \(error.localizedDescription)")
    }
}
