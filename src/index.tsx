import {
  EmitterSubscription,
  NativeEventEmitter,
  NativeModules,
  Platform,
} from 'react-native';
import { PlatformGuard } from './decorators';

const LINKING_ERROR =
  `The package 'react-native-audio-manager-ios' doesn't seem to be linked. Make sure: \n\n` +
  Platform.select({ ios: "- You have run 'pod install'\n", default: '' }) +
  '- You rebuilt the app after installing the package\n' +
  '- You are not using Expo Go\n';

const AudioManagerModule = NativeModules.AudioManager
  ? NativeModules.AudioManager
  : new Proxy(
      {},
      {
        get() {
          throw new Error(LINKING_ERROR);
        },
      }
    );

const AudioManagerEmitter = new NativeEventEmitter(AudioManagerModule);

export interface IRouteInfo {
  id: string;
  name: string;
  type: TAudioRoute;
  isSelected: boolean;
}

export interface IDeviceInfo {
  id: string;
  name: string;
  type: TAudioRoute;
}

export type TAudioRoute =
  | 'EARPIECE'
  | 'SPEAKER_PHONE'
  | 'BLUETOOTH'
  | 'WIRED_HEADSET';

export type TStrategy = 'NORMAL' | 'SCO';

export interface IBaseSubscription {
  action: keyof TEventListenerActionData;
  subscription: EmitterSubscription;
  callback: (
    data: TEventListenerActionData[keyof TEventListenerActionData]
  ) => void;
}

export type TEventListenerActionData = {
  onRouteAdded: IRouteInfo;
  onRouteRemoved: IRouteInfo;
  onRouteSelected: IRouteInfo;
  onRouteUnselected: IRouteInfo;
  onAudioDeviceChanged: IDeviceInfo[];
};

class AudioManagerService {
  private isAndroid = Platform.OS === 'android';
  private subscriptions: IBaseSubscription[] = [];

  /**
   * @description Start AudioManager service
   * @param strategy:  'NORMAL' | 'SCO' - Only android
   */
  public async start(strategy: TStrategy = 'SCO') {
    await AudioManagerModule.start(...(this.isAndroid ? [strategy] : []));
  }

  /**
   * @description Stop AudioManager service
   */
  public stop() {
    AudioManagerModule.stop();
    this.removeAllListeners();
  }

  /**
   * @param route TAudioRoute
   * @param strategy:  'NORMAL' | 'SCO' - Only android
   */
  public chooseAudioRoute(route: TAudioRoute, strategy: TStrategy = 'SCO') {
    AudioManagerModule.chooseAudioRoute(
      ...(this.isAndroid ? [route, strategy] : [route])
    );
  }

  /**
   * @description Installing priority route if bluetooth available setup bluetooth
   * if wired available setup bluetooth else setup speaker phone
   * @description Only Android Platform
   */
  @PlatformGuard('android')
  public async installRoutByPriority() {
    return await AudioManagerModule.installRoutByPriority();
  }

  /**
   * @description Only Android Platform
   * @return List audio routes from system
   */
  @PlatformGuard('android')
  public async getRoutes() {
    return (await AudioManagerModule.getRoutes()) as IRouteInfo[];
  }

  /**
   * @return List audio devices from system
   * @description Android routes and Android devices different units
   */
  public async getDevices() {
    return (await AudioManagerModule.getDevices()) as IDeviceInfo[];
  }

  /**
   * @param enabled
   * @description Enable bluetooth SCO mode
   * @description Only Android Platform
   */
  @PlatformGuard('android')
  public setBluetoothScoOn(enabled: boolean) {
    AudioManagerModule.setBluetoothScoOn(enabled);
  }

  @PlatformGuard('ios')
  public async startMicRecording() {
    await AudioManagerModule.startMicRecording();
  }

  @PlatformGuard('ios')
  public async stopMicRecording() {
    await AudioManagerModule.stopMicRecording();
  }

  /**
   * @param action
   * @param callback
   */
  public addEventListener<
    K extends keyof TEventListenerActionData = keyof TEventListenerActionData
  >(action: K, callback: (data: TEventListenerActionData[K]) => void) {
    const subscription = AudioManagerEmitter.addListener(
      action as string,
      callback
    );
    this.subscriptions.push({
      action,
      subscription,
      callback: callback as (
        data: TEventListenerActionData[keyof TEventListenerActionData]
      ) => void,
    });
  }

  /**
   * @param callback
   */
  public removeEventListener<
    T extends TEventListenerActionData[keyof TEventListenerActionData]
  >(callback: (data: T) => void) {
    this.subscriptions = this.subscriptions.reduce<IBaseSubscription[]>(
      (acc, item) => {
        if (item.callback === callback) {
          item.subscription.remove();
        } else {
          acc.push(item);
        }
        return acc;
      },
      []
    );
  }

  /**
   * @description Remove all listeners
   */
  removeAllListeners(): void;
  /**
   * @param event
   * @description Remove all listeners from event
   */
  removeAllListeners(event: keyof TEventListenerActionData): void;
  public removeAllListeners(event?: keyof TEventListenerActionData) {
    if (event) {
      this.subscriptions = this.subscriptions.reduce<IBaseSubscription[]>(
        (acc, item) => {
          if (item.action === event) {
            AudioManagerEmitter.removeAllListeners(event);
          } else {
            acc.push(item);
          }
          return acc;
        },
        []
      );
    } else {
      this.subscriptions.forEach(({ subscription }) => {
        subscription.remove();
      });

      this.subscriptions = [];
    }
  }
}

export const AudioManager = new AudioManagerService();
