import { Platform } from 'react-native';

export function PlatformGuard(onlyPlatform: typeof Platform.OS) {
  return function (_: unknown, __: string, descriptor: PropertyDescriptor) {
    const method = descriptor.value!;

    descriptor.value = function (...args: []) {
      if (Platform.OS === onlyPlatform) {
        return method.apply(this, args);
      }
      return Promise.resolve();
    };
  };
}
