# react-native-audio-manager

Audio routes manager for React Native

## Installation

```sh
npm install react-native-audio-manager
```

## Usage

```js
import { AudioManager } from 'react-native-audio-manager';

// Start service and intercept audio settings change from other apps
AudioManager.start()

// Set audio output
// device: TPreferredDeviceType = 'EARPIECE' | 'SPEAKER_PHONE' | 'BLUETOOTH' | 'WIRED_HEADSET';
AudioManager.chooseAudioRoute(device);

// Installing priority route if bluetooth available setup bluetooth
// wired available setup bluetooth else setup speaker phone
// Only Android Platform
AudioManager.installRoutByPriority();

// Get devices from system
// device: IDeviceInfo;
AudioManager.getDevices();

// Get routes from system
// route: IRouteInfo;
// Android only
AudioManager.getRoutes();

// Listeners

// Add listener for event;
// event: 'onRouteAdded' | 'onRouteRemoved' | 'onRouteSelected' |  'onRouteUnselected' | 'onAudioDeviceChanged';
AudioManager.addEventListener(event, listener);

// Remove event
// callback:  The same link to the callback as when subscribing
AudioManager.removeEventListener(listener);

// Remove all events
// event?: 'onRouteAdded' | 'onRouteRemoved' | 'onRouteSelected' |  'onRouteUnselected' | 'onAudioDeviceChanged' | undefined
// If an argument is passed, removes all listeners from event, else remove all listeners from all events
AudioManager.removeAllListeners(event);

// Start service;
// Removed all listeners
AudioManager.stop()
```

## Contributing

See the [contributing guide](CONTRIBUTING.md) to learn how to contribute to the repository and the development workflow.

## License

MIT
