package com.audiomanager;

import android.annotation.SuppressLint;
import android.media.AudioFormat;
import android.media.AudioRecord;
import android.media.MediaRecorder.AudioSource;
import android.util.Log;

public class AudioRecorder {
  private AudioRecord recorder;
  private static final String TAG = "AudioRecorder";
  private static final int RECORDER_SAMPLE_RATE = 44100;
  private static final int RECORDER_BUFFER_SIZE = (int) ((double)RECORDER_SAMPLE_RATE*0.1*2); // 44100*0.1;


  @SuppressLint("MissingPermission")
  public void start() {
    // STOP CURRENT RECORDER
    stop();

    // CREATE NEW INSTANCE
    recorder = new AudioRecord(AudioSource.MIC, RECORDER_SAMPLE_RATE, AudioFormat.CHANNEL_IN_MONO, AudioFormat.ENCODING_PCM_16BIT, RECORDER_BUFFER_SIZE);

    if (recorder.getState() == AudioRecord.STATE_INITIALIZED) {
      Log.d(TAG,"initialized successfully");
    } else {
      Log.e(TAG,"failed to initialize");
      return;
    }

    try {
      recorder.startRecording();
      Log.e(TAG,"start recording successfully");
    } catch (IllegalStateException e) {
      Log.e(TAG,"failed to start recording");
    }
  }

  public void stop() {
    if (recorder != null) {
      recorder.stop();
      recorder.release();
      recorder = null;
      Log.e(TAG,"stop success");
    }
  }
}
