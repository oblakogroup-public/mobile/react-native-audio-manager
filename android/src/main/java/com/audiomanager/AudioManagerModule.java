package com.audiomanager;

import androidx.annotation.NonNull;
import androidx.annotation.MainThread;

import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.BroadcastReceiver;

import android.media.AudioAttributes;
import android.media.AudioDeviceCallback;
import android.media.AudioDeviceInfo;
import android.media.AudioFocusRequest;
import android.media.AudioManager;

import androidx.annotation.RequiresApi;
import androidx.mediarouter.media.MediaRouter;
import androidx.mediarouter.media.MediaRouter.RouteInfo;
import androidx.mediarouter.media.MediaRouteSelector;
import androidx.mediarouter.media.MediaControlIntent;

import android.bluetooth.BluetoothHeadset;


import android.os.Build;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;

import com.facebook.react.bridge.Arguments;
import com.facebook.react.bridge.LifecycleEventListener;
import com.facebook.react.bridge.NativeModule;
import com.facebook.react.bridge.Promise;
import com.facebook.react.bridge.ReactApplicationContext;
import com.facebook.react.bridge.ReactContext;
import com.facebook.react.bridge.ReactContextBaseJavaModule;
import com.facebook.react.bridge.ReactMethod;
import com.facebook.react.bridge.WritableMap;
import com.facebook.react.bridge.WritableArray;
import com.facebook.react.bridge.UiThreadUtil;
import com.facebook.react.module.annotations.ReactModule;
import com.facebook.react.modules.core.DeviceEventManagerModule;

import java.lang.Runnable;
import java.util.List;
import java.util.HashSet;
import java.util.Set;
import java.util.HashMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.Timer;

@ReactModule(name = AudioManagerModule.NAME)
public class AudioManagerModule extends ReactContextBaseJavaModule implements AudioManager.OnAudioFocusChangeListener {
  public static final String NAME = "AudioManager";
  private static final String TAG = NAME;
  private static final String ROUTE_ADDED_EVENT_NAME = "onRouteAdded";
  private static final String ROUTE_REMOVED_EVENT_NAME = "onRouteRemoved";
  private static final String ROUTE_SELECTED_EVENT_NAME = "onRouteSelected";
  private static final String ROUTE_UNSELECTED_EVENT_NAME = "onRouteUnselected";
  private static final String DEVICE_CHANGED_EVENT_NAME = "onAudioDeviceChanged";
  private final static int HEADSET_PLUGGED = 1;
  private final static int HEADSET_UNPLUGGED = 0;
  private String CURRENT_SELECTED_STRATEGY = "SCO";

  // AudioAttributes
  private AudioAttributes mAudioAttributes;
  private AudioFocusRequest mAudioFocusRequest;

  // AudioRouter
  private AudioManager audioManager;
  private AudioRecorder audioRecorder;
  // MediaRouter
  private MediaRouter mediaRouter;
  private MediaRouter.Callback mediaRouterCallback;

  // BluetoothReceiver
  private final BroadcastReceiver headsetReceiver;

  // Common variables
  private final ReactApplicationContext reactContext;
  private final ExecutorService executor = Executors.newSingleThreadExecutor();
  private final Handler handler = new Handler(Looper.getMainLooper());
  private final int DEVICE_TYPE_BLUETOOTH = 3;
  private Timer scoStatusTimer;
  private boolean wiredHeadsetIsPlugged = false;

  public enum AudioDevice {SPEAKER_PHONE, WIRED_HEADSET, EARPIECE, BLUETOOTH, NONE}


  public AudioManagerModule(ReactApplicationContext reactContext) {
    super(reactContext);
    this.reactContext = reactContext;

    handler.post(() -> {
      mediaRouter = MediaRouter.getInstance(reactContext);
    });

    audioManager = ((AudioManager) reactContext.getSystemService(Context.AUDIO_SERVICE));
    headsetReceiver = new HeadsetBroadcastReceiver();

    Log.d(TAG, TAG + "- initialized");
  }

  @Override
  @NonNull
  public String getName() {
    return NAME;
  }


  // REACT METHODS
  @ReactMethod
  public void start(String strategy, Promise promise) {
    Log.d(TAG, "____SERVICE_STARTED_____");

    CURRENT_SELECTED_STRATEGY = strategy;

    IntentFilter headsetFilter = new IntentFilter();
    headsetFilter.addAction(BluetoothHeadset.ACTION_CONNECTION_STATE_CHANGED);
    headsetFilter.addAction(Intent.ACTION_HEADSET_PLUG);

    try {
      reactContext.registerReceiver(headsetReceiver, headsetFilter);
    } catch (Exception e) {
      e.printStackTrace();
    }

    handler.post(() -> {
      mediaRouterCallback = new MediaRouterCallback();
      MediaRouteSelector mSelector = new MediaRouteSelector.Builder()
        .addControlCategory(MediaControlIntent.CATEGORY_LIVE_AUDIO)
        .build();
      mediaRouter.addCallback(mSelector, mediaRouterCallback);
    });

    audioManager.registerAudioDeviceCallback(audioDeviceCallback, null);
    audioManager.setMicrophoneMute(false);

    if (strategy.equals("SCO")) {
      initAudioRouteThisSco(promise);
    } else {
      initAudioRoute(promise);
    }
  }

  @ReactMethod
  public void stop() {
    Log.d(TAG, "____SERVICE_STOPPED_____");

    if (scoStatusTimer != null) {
      scoStatusTimer.cancel();
      scoStatusTimer = null;
    }

    audioManager.unregisterAudioDeviceCallback(audioDeviceCallback);

    abandonAudioFocus();

    if (headsetReceiver != null) {
      try {
        reactContext.unregisterReceiver(headsetReceiver);
      } catch (Exception e) {
        e.printStackTrace();
      }
    }

    if (mediaRouterCallback != null && mediaRouter != null) {
      handler.post(() -> {
        mediaRouter.removeCallback(mediaRouterCallback);
      });
    }
  }

  @ReactMethod
  public void chooseAudioRoute(String audioRoute, String strategy) {
    Log.d(TAG, "USER CHOSEN AUDIO ROUTE" + audioRoute);

    requestAudioFocus();

    CURRENT_SELECTED_STRATEGY = strategy;

    if (strategy.equals("SCO")) {
      setMode(AudioManager.MODE_IN_COMMUNICATION);
      chooseAudioRouteWithSco(audioRoute);
    } else {
      setMode(AudioManager.MODE_NORMAL);
      handler.post(() -> {
        setAudioRouteFromRoutes(audioRoute);
      });
    }
  }

  @ReactMethod
  public void installRoutByPriority(Promise promise) {
    if (CURRENT_SELECTED_STRATEGY.equals("SCO")) {
      initAudioRouteThisSco(promise);
    } else {
      initAudioRoute(promise);
    }
  }

  @ReactMethod
  public void getDevices(Promise promise) {
    AudioDeviceInfo[] devices = audioManager.getDevices(AudioManager.GET_DEVICES_OUTPUTS);
    promise.resolve(createJSDevices(devices));
  }

  @ReactMethod
  public void getRoutes(Promise promise) {
    handler.post(() -> {
      List<RouteInfo> routes = mediaRouter.getRoutes();
      promise.resolve(createJSRoutes(routes));
    });
  }

  @ReactMethod
  public void setBluetoothScoOn(boolean enabled) {
    _setBluetoothScoOn(enabled);
  }

  @ReactMethod
  public void requestAudioFocusJS(Promise promise) {
    promise.resolve(requestAudioFocus());
  }

  @ReactMethod
  public void isWiredHeadsetPluggedIn(Promise promise) {
    promise.resolve(hasWiredHeadset());
  }

  @ReactMethod
  public void startMicRecording(Promise promise) {
    Thread streamThread = new Thread(() -> {
      if (audioRecorder != null) {
        audioRecorder.stop();
        audioRecorder = null;
      }

      audioRecorder = new AudioRecorder();
      audioRecorder.start();
    });

    streamThread.start();
    promise.resolve(true);
  }

  @ReactMethod
  public void stopMicRecording(Promise promise) {
    Thread stopThread = new Thread(() -> {
      if (audioRecorder != null) {
        audioRecorder.stop();
        audioRecorder = null;
      }
    });

    stopThread.start();
    promise.resolve(false);
  }

  // COMMON METHODS
  private void selectAudioRoute(MediaRouter.RouteInfo route, boolean isSpeakerPhone) {
    audioManager.setMicrophoneMute(false);
    audioManager.setSpeakerphoneOn(isSpeakerPhone);
    route.select();
  }

  private void setMode(int mode) {
    audioManager.setMode(mode);
  }

  public void initAudioRouteThisSco(Promise jsPromise) {
    setMode(AudioManager.MODE_IN_COMMUNICATION);
    requestAudioFocus();

    String currentRoute = AudioDevice.SPEAKER_PHONE.name();

    if (hasBluetoothDevices()) {
      currentRoute = AudioDevice.BLUETOOTH.name();
    } else if (hasWiredHeadset()) {
      currentRoute = AudioDevice.WIRED_HEADSET.name();
    }

    chooseAudioRouteWithSco(currentRoute);

    AudioDeviceInfo[] devices = audioManager.getDevices(AudioManager.GET_DEVICES_OUTPUTS);
    WritableMap data = Arguments.createMap();
    data.putString("selectedDevice", currentRoute);
    data.putArray("devices", createJSDevices(devices));

    jsPromise.resolve(data);
  }

  public void initAudioRoute(Promise jsPromise) {
    setMode(AudioManager.MODE_NORMAL);
    requestAudioFocus();

    handler.post(() -> {
      List<RouteInfo> routes = mediaRouter.getRoutes();
      HashMap<String, RouteInfo> routesMap = getRoutesHashMap(routes);
      RouteInfo bluetoothRoute = routesMap.get(AudioDevice.BLUETOOTH.name());
      RouteInfo speakerRoute = routesMap.get(AudioDevice.SPEAKER_PHONE.name());
      RouteInfo wiredRoute = routesMap.get(AudioDevice.WIRED_HEADSET.name());
      String selectedRoute = AudioDevice.NONE.name();

      if (bluetoothRoute != null) {
        selectAudioRoute(bluetoothRoute, false);
        selectedRoute = AudioDevice.BLUETOOTH.name();
      } else if (wiredRoute != null) {
        selectAudioRoute(wiredRoute, false);
        selectedRoute = AudioDevice.WIRED_HEADSET.name();
      } else if (speakerRoute != null) {
        selectAudioRoute(speakerRoute, true);
        selectedRoute = AudioDevice.SPEAKER_PHONE.name();
      }

      AudioDeviceInfo[] devices = audioManager.getDevices(AudioManager.GET_DEVICES_OUTPUTS);
      WritableMap data = Arguments.createMap();
      data.putString("selectedDevice", selectedRoute);
      data.putArray("devices", createJSDevices(devices));

      jsPromise.resolve(data);
    });
  }

  private void handleRouteChanged(String action, RouteInfo route) {
    logRouteInfo("MediaRouterCallback onRoute" + action + ": ", route);
    requestAudioFocus();

    boolean isBluetoothRoute = route.getDeviceType() == DEVICE_TYPE_BLUETOOTH;

    if (action.equals("ADDED")) {
      emitEvent(ROUTE_ADDED_EVENT_NAME, createJSRouteObject(route));

      if (isBluetoothRoute) {
        if (CURRENT_SELECTED_STRATEGY.equals("SCO")) {
          chooseAudioRouteWithSco(AudioDevice.BLUETOOTH.name());
        } else {
          selectAudioRoute(route, false);
        }
      }
    } else {
      emitEvent(ROUTE_REMOVED_EVENT_NAME, createJSRouteObject(route));

      if (isBluetoothRoute && !hasWiredHeadset()) {
        if (CURRENT_SELECTED_STRATEGY.equals("SCO")) {
          if (hasWiredHeadset()) {
            chooseAudioRouteWithSco(AudioDevice.WIRED_HEADSET.name());
          } else {
            chooseAudioRouteWithSco(AudioDevice.SPEAKER_PHONE.name());
          }
        } else {
          if (!hasWiredHeadset()) {
            audioManager.setSpeakerphoneOn(true);
          }
        }
      }
    }
  }

  private void handleWiredHeadsetPluggedIn(boolean isPlugging) {
    if (isPlugging) {
      if (!wiredHeadsetIsPlugged) {
        Log.d(TAG, "ACTION  : " + "WeiredHeadset - HEADSET_PLUGGED");
        wiredHeadsetIsPlugged = true;

        if (CURRENT_SELECTED_STRATEGY.equals("SCO")) {
          chooseAudioRouteWithSco(AudioDevice.WIRED_HEADSET.name());
        } else {
          handler.post(() -> {
            setAudioRouteFromRoutes(AudioDevice.WIRED_HEADSET.name());
          });
        }
      }
    } else {
      if (wiredHeadsetIsPlugged) {
        Log.d(TAG, "ACTION  : " + "WeiredHeadset - HEADSET_UNPLUGGED");
        wiredHeadsetIsPlugged = false;

        if (CURRENT_SELECTED_STRATEGY.equals("SCO")) {
          chooseAudioRouteWithSco(AudioDevice.SPEAKER_PHONE.name());
        } else {
          handler.post(() -> {
            setAudioRouteFromRoutes(AudioDevice.SPEAKER_PHONE.name());
          });
        }
      }
    }
  }

  @MainThread
  private void setAudioRouteFromRoutes(String audioRoute) {
    Log.d(TAG, "setAudioRouteFromRoutes route: " + audioRoute);

    List<RouteInfo> routes = mediaRouter.getRoutes();
    HashMap<String, RouteInfo> routesMap = getRoutesHashMap(routes);
    RouteInfo selectedRoute = routes.stream()
      .filter(route -> route.isSelected())
      .findAny()
      .orElse(null);

    if (audioRoute.equals(AudioDevice.BLUETOOTH.name()) && routesMap.containsKey(AudioDevice.BLUETOOTH.name())) {
      RouteInfo bluetoothRoute = routesMap.get(AudioDevice.BLUETOOTH.name());
      selectAudioRoute(bluetoothRoute, false);
    } else if (audioRoute.equals(AudioDevice.SPEAKER_PHONE.name())) {
      RouteInfo speakerRoute = routesMap.get(AudioDevice.SPEAKER_PHONE.name());
      RouteInfo wiredRoute = routesMap.get(AudioDevice.WIRED_HEADSET.name());

      if (speakerRoute != null) {
        selectAudioRoute(speakerRoute, true);
      } else if (wiredRoute != null) {
        selectAudioRoute(wiredRoute, true);
      }
    } else if (audioRoute.equals(AudioDevice.WIRED_HEADSET.name()) || audioRoute.equals(AudioDevice.EARPIECE.name())) {
      if (selectedRoute != null) {
        if (selectedRoute.isBluetooth()) {
          RouteInfo wiredRoute = routesMap.get(AudioDevice.WIRED_HEADSET.name());
          RouteInfo speakerRoute = routesMap.get(AudioDevice.SPEAKER_PHONE.name());

          if (wiredRoute != null) {
            selectAudioRoute(wiredRoute, false);
          } else if (speakerRoute != null) {
            selectAudioRoute(speakerRoute, false);
          }
        } else {
          audioManager.setSpeakerphoneOn(false);
        }
      }
    }
  }

  private HashMap<String, RouteInfo> getRoutesHashMap(List<RouteInfo> routes) {
    HashMap<String, RouteInfo> routesMap = new HashMap();

    for (RouteInfo route : routes) {
      String correctType = AudioDevice.NONE.name();

      if (route.isBluetooth()) {
        correctType = AudioDevice.BLUETOOTH.name();
      } else if (route.isDeviceSpeaker()) {
        correctType = AudioDevice.SPEAKER_PHONE.name();
      } else if (!route.isDeviceSpeaker()) {
        correctType = AudioDevice.WIRED_HEADSET.name();
      }

      routesMap.put(correctType, route);
    }

    return routesMap;
  }

  private String getCurrentSelectedDevice() {
    boolean hasBluetooth = hasBluetoothDevices();
    Log.d(TAG, "BLUETOOTH has DEVICES: " + hasBluetooth);

    String currentRoute = AudioDevice.NONE.name();

    if (hasBluetoothDevices() && audioManager.isBluetoothScoOn()) {
      currentRoute = AudioDevice.BLUETOOTH.name();
    } else if (hasWiredHeadset()) {
      currentRoute = AudioDevice.WIRED_HEADSET.name();
    } else if (audioManager.isSpeakerphoneOn()) {
      currentRoute = AudioDevice.SPEAKER_PHONE.name();
    } else {
      currentRoute = AudioDevice.EARPIECE.name();
    }

    Log.d(TAG, "SELECTED ROUTE: " + currentRoute);

    return currentRoute;
  }

  private boolean hasBluetoothDevices() {
    final AudioDeviceInfo[] devices = audioManager.getDevices(AudioManager.GET_DEVICES_OUTPUTS);

    for (AudioDeviceInfo device : devices) {
      if (device.getType() == AudioDeviceInfo.TYPE_BLUETOOTH_SCO) {
        Log.d(TAG, "BluetoothHeadset: found");
        return true;
      }
    }

    return false;
  }

  private boolean hasWiredHeadset() {
    final AudioDeviceInfo[] devices = audioManager.getDevices(AudioManager.GET_DEVICES_OUTPUTS);

    for (AudioDeviceInfo device : devices) {
      final int type = device.getType();
      if (type == AudioDeviceInfo.TYPE_WIRED_HEADSET) {
        Log.d(TAG, "hasWiredHeadset: found wired headset");
        return true;
      } else if (type == AudioDeviceInfo.TYPE_USB_DEVICE) {
        Log.d(TAG, "hasWiredHeadset: found USB audio device");
        return true;
      } else if (type == AudioDeviceInfo.TYPE_WIRED_HEADPHONES) {
        Log.d(TAG, "hasWiredHeadset: found wired headphones");
        return true;
      }
    }
    return false;
  }

  private String getCorrectDeviceType(int type) {
    if (type == AudioDeviceInfo.TYPE_WIRED_HEADSET || type == AudioDeviceInfo.TYPE_USB_DEVICE || type == AudioDeviceInfo.TYPE_WIRED_HEADPHONES) {
      return AudioDevice.WIRED_HEADSET.name();
    } else if (type == AudioDeviceInfo.TYPE_BLUETOOTH_SCO) {
      return AudioDevice.BLUETOOTH.name();
    } else if (type == AudioDeviceInfo.TYPE_BUILTIN_EARPIECE) {
      return AudioDevice.EARPIECE.name();
    } else if (type == AudioDeviceInfo.TYPE_BUILTIN_SPEAKER) {
      return AudioDevice.SPEAKER_PHONE.name();
    } else {
      return AudioDevice.NONE.name();
    }
  }

  public void chooseAudioRouteWithSco(String audioRoute) {
    audioManager.setSpeakerphoneOn(audioRoute.equals(AudioDevice.SPEAKER_PHONE.name()));
    _setBluetoothScoOn(audioRoute.equals(AudioDevice.BLUETOOTH.name()));
  }

  private void logRouteInfo(String action, RouteInfo route) {
    Log.d(TAG, action
      + "NAME= " + route.getName()
      + ", DEVICE TYPE= " + route.getDeviceType()
      + ", isDefault= " + route.isDefault()
      + ", isBluetooth= " + route.isBluetooth()
      + ", isEnabled= " + route.isEnabled()
      + ", isDeviceSpeaker= " + route.isDeviceSpeaker()
      + ", isSelected= " + route.isSelected());
  }

  private void _setBluetoothScoOn(boolean enabled) {
    if (enabled) {
      audioManager.startBluetoothSco();
      audioManager.setBluetoothScoOn(true);
    } else {
      audioManager.setBluetoothScoOn(false);
      audioManager.stopBluetoothSco();
    }
  }

  private String requestAudioFocus() {
    String requestAudioFocusResStr = (android.os.Build.VERSION.SDK_INT >= 26)
      ? requestAudioFocusV26()
      : requestAudioFocusOld();
    Log.d(TAG, "requestAudioFocus(): " + requestAudioFocusResStr);
    return requestAudioFocusResStr;
  }

  @RequiresApi(api = Build.VERSION_CODES.O)
  private String requestAudioFocusV26() {
    if (mAudioAttributes == null) {
      mAudioAttributes = new AudioAttributes.Builder()
        .setUsage(AudioAttributes.USAGE_VOICE_COMMUNICATION)
        .setContentType(AudioAttributes.CONTENT_TYPE_SPEECH)
        .build();
    }

    if (mAudioFocusRequest == null) {
      mAudioFocusRequest = new AudioFocusRequest.Builder(AudioManager.AUDIOFOCUS_GAIN)
        .setAudioAttributes(mAudioAttributes)
        .setAcceptsDelayedFocusGain(false)
        .setOnAudioFocusChangeListener(this)
        .build();
    }

    int requestAudioFocusRes = audioManager.requestAudioFocus(mAudioFocusRequest);

    String requestAudioFocusResStr;
    switch (requestAudioFocusRes) {
      case AudioManager.AUDIOFOCUS_REQUEST_FAILED:
        requestAudioFocusResStr = "AUDIOFOCUS_REQUEST_FAILED";
        break;
      case AudioManager.AUDIOFOCUS_REQUEST_GRANTED:
        requestAudioFocusResStr = "AUDIOFOCUS_REQUEST_GRANTED";
        break;
      case AudioManager.AUDIOFOCUS_REQUEST_DELAYED:
        requestAudioFocusResStr = "AUDIOFOCUS_REQUEST_DELAYED";
        break;
      default:
        requestAudioFocusResStr = "AUDIOFOCUS_REQUEST_UNKNOWN";
        break;
    }

    return requestAudioFocusResStr;
  }

  private String requestAudioFocusOld() {
    int requestAudioFocusRes = audioManager.requestAudioFocus(this, AudioManager.STREAM_VOICE_CALL, AudioManager.AUDIOFOCUS_GAIN);

    String requestAudioFocusResStr;
    switch (requestAudioFocusRes) {
      case AudioManager.AUDIOFOCUS_REQUEST_FAILED:
        requestAudioFocusResStr = "AUDIOFOCUS_REQUEST_FAILED";
        break;
      case AudioManager.AUDIOFOCUS_REQUEST_GRANTED:
        requestAudioFocusResStr = "AUDIOFOCUS_REQUEST_GRANTED";
        break;
      default:
        requestAudioFocusResStr = "AUDIOFOCUS_REQUEST_UNKNOWN";
        break;
    }

    return requestAudioFocusResStr;
  }

  private String abandonAudioFocus() {
    String abandonAudioFocusResStr = (android.os.Build.VERSION.SDK_INT >= 26)
      ? abandonAudioFocusV26()
      : abandonAudioFocusOld();
    Log.d(TAG, "abandonAudioFocus(): " + abandonAudioFocusResStr);
    return abandonAudioFocusResStr;
  }

  @RequiresApi(api = Build.VERSION_CODES.O)
  private String abandonAudioFocusV26() {
    int abandonAudioFocusRes = mAudioFocusRequest != null ? audioManager.abandonAudioFocusRequest(mAudioFocusRequest): AudioManager.AUDIOFOCUS_REQUEST_FAILED;

    String abandonAudioFocusResStr;

    switch (abandonAudioFocusRes) {
      case AudioManager.AUDIOFOCUS_REQUEST_FAILED:
        abandonAudioFocusResStr = "AUDIOFOCUS_REQUEST_FAILED";
        break;
      case AudioManager.AUDIOFOCUS_REQUEST_GRANTED:
        abandonAudioFocusResStr = "AUDIOFOCUS_REQUEST_GRANTED";
        break;
      default:
        abandonAudioFocusResStr = "AUDIOFOCUS_REQUEST_UNKNOWN";
        break;
    }

    return abandonAudioFocusResStr;
  }

  private String abandonAudioFocusOld() {
    int abandonAudioFocusRes = audioManager.abandonAudioFocus(this);

    String abandonAudioFocusResStr;
    switch (abandonAudioFocusRes) {
      case AudioManager.AUDIOFOCUS_REQUEST_FAILED:
        abandonAudioFocusResStr = "AUDIOFOCUS_REQUEST_FAILED";
        break;
      case AudioManager.AUDIOFOCUS_REQUEST_GRANTED:
        abandonAudioFocusResStr = "AUDIOFOCUS_REQUEST_GRANTED";
        break;
      default:
        abandonAudioFocusResStr = "AUDIOFOCUS_REQUEST_UNKNOWN";
        break;
    }

    return abandonAudioFocusResStr;
  }

  // REACT UTILITY METHODS
  private void emitEvent(String eventName, Object data) {
    if (!reactContext.hasActiveCatalystInstance()) {
      return;
    }

    executor.execute(() -> {
      reactContext
        .getJSModule(DeviceEventManagerModule.RCTDeviceEventEmitter.class)
        .emit(eventName, data);
    });
  }

  private WritableArray createJSDevices(AudioDeviceInfo[] devices) {
    WritableArray allDeviceInfos = Arguments.createArray();

    Set<Integer> checks = new HashSet<Integer>() {{
      add(AudioDeviceInfo.TYPE_BLUETOOTH_SCO);
      add(AudioDeviceInfo.TYPE_BUILTIN_EARPIECE);
      add(AudioDeviceInfo.TYPE_BUILTIN_SPEAKER);
      add(AudioDeviceInfo.TYPE_WIRED_HEADPHONES);
      add(AudioDeviceInfo.TYPE_WIRED_HEADSET);
      add(AudioDeviceInfo.TYPE_USB_DEVICE);
    }};

    for (AudioDeviceInfo device : devices) {
      if (checks.contains(device.getType())) {
        allDeviceInfos.pushMap(createJSDeviceObject(device));
      }
    }

    return allDeviceInfos;
  }

  private WritableArray createJSRoutes(List<RouteInfo> routes) {
    WritableArray allRouteInfos = Arguments.createArray();

    for (RouteInfo route : routes) {
      logRouteInfo("createJSRoutes ROUTE: ", route);
      allRouteInfos.pushMap(createJSRouteObject(route));
    }

    return allRouteInfos;
  }

  private WritableMap createJSRouteObject(RouteInfo route) {
    WritableMap routeInfo = Arguments.createMap();
    int originType = route.getDeviceType();
    String routeType = AudioDevice.NONE.name();

    if (originType == DEVICE_TYPE_BLUETOOTH) {
      routeType = AudioDevice.BLUETOOTH.name();
    } else if (originType == RouteInfo.DEVICE_TYPE_SPEAKER) {
      routeType = AudioDevice.SPEAKER_PHONE.name();
    }

    routeInfo.putString("id", route.getId());
    routeInfo.putString("name", route.getName());
    routeInfo.putString("type", routeType);
    routeInfo.putBoolean("isSelected", route.isSelected());

    return routeInfo;
  }

  private WritableMap createJSDeviceObject(AudioDeviceInfo device) {
    final int type = device.getType();
    final int id = device.getId();
    final String name = device.getProductName().toString();
    WritableMap deviceInfo = Arguments.createMap();

    deviceInfo.putString("type", getCorrectDeviceType(type));
    deviceInfo.putString("name", name);
    deviceInfo.putString("id", Integer.toString(id));

    return deviceInfo;
  }


  // CLASSES, RUNNABLES, TIMERS
  private class MediaRouterCallback extends MediaRouter.Callback {
    @Override
    public void onRouteAdded(MediaRouter router, RouteInfo route) {
      handleRouteChanged("ADDED", route);
    }

    @Override
    public void onRouteRemoved(MediaRouter router, RouteInfo route) {
      handleRouteChanged("REMOVED", route);
    }

    @Override
    public void onRouteSelected(MediaRouter router, RouteInfo route) {
      logRouteInfo("MediaRouterCallback onRouteSelected: ", route);
      emitEvent(ROUTE_SELECTED_EVENT_NAME, createJSRouteObject(route));
    }
  }

  private class HeadsetBroadcastReceiver extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
      final String action = intent.getAction();

      if (action.equals(BluetoothHeadset.ACTION_CONNECTION_STATE_CHANGED)) {
        Log.d(TAG, "ACTION  : " + "BluetoothHeadset - ACTION_CONNECTION_STATE_CHANGED");
        final int state = intent.getIntExtra(BluetoothHeadset.EXTRA_STATE, BluetoothHeadset.STATE_DISCONNECTED);

        if (state == BluetoothHeadset.STATE_CONNECTED) {
          Log.d(TAG, "BT STATE : " + "--STATE_CONNECTED");
        } else if (state == BluetoothHeadset.STATE_DISCONNECTED) {
          Log.d(TAG, "BT STATE : " + "--STATE_DISCONNECTED");
        }
      } else if (action.equals(Intent.ACTION_HEADSET_PLUG)) {
        Log.d(TAG, "ACTION  : " + "WeiredHeadset - ACTION_HEADSET_PLUG");
        final int state = intent.getIntExtra("state", -1);

        handleWiredHeadsetPluggedIn(state == HEADSET_PLUGGED);
      }
    }
  }

  private final AudioDeviceCallback audioDeviceCallback = new AudioDeviceCallback() {
      @Override
      public void onAudioDevicesAdded(AudioDeviceInfo[] addedDevices) {
        Log.d(TAG, "DEVICE_ADDED_________");
        executor.execute(onAudioDeviceChangeRunner);
      }

      @Override
      public void onAudioDevicesRemoved(AudioDeviceInfo[] removedDevices) {
        Log.d(TAG, "DEVICE_REMOVED_________");
        executor.execute(onAudioDeviceChangeRunner);
      }
    };

  private final Runnable onAudioDeviceChangeRunner = new Runnable() {
    @Override
    public void run() {
      AudioDeviceInfo[] devices = audioManager.getDevices(AudioManager.GET_DEVICES_OUTPUTS);

      WritableArray devicesMap = createJSDevices(devices);
      Log.d(TAG, "DEVICE_UPDATED_________");
      emitEvent(DEVICE_CHANGED_EVENT_NAME, devicesMap);
    }
  };

  @Override
  public void onAudioFocusChange(int focusChange) {
    String focusChangeStr;
    switch (focusChange) {
      case AudioManager.AUDIOFOCUS_GAIN:
        focusChangeStr = "AUDIOFOCUS_GAIN";
        break;
      case AudioManager.AUDIOFOCUS_GAIN_TRANSIENT:
        focusChangeStr = "AUDIOFOCUS_GAIN_TRANSIENT";
        break;
      case AudioManager.AUDIOFOCUS_GAIN_TRANSIENT_EXCLUSIVE:
        focusChangeStr = "AUDIOFOCUS_GAIN_TRANSIENT_EXCLUSIVE";
        break;
      case AudioManager.AUDIOFOCUS_GAIN_TRANSIENT_MAY_DUCK:
        focusChangeStr = "AUDIOFOCUS_GAIN_TRANSIENT_MAY_DUCK";
        break;
      case AudioManager.AUDIOFOCUS_LOSS:
        focusChangeStr = "AUDIOFOCUS_LOSS";
        break;
      case AudioManager.AUDIOFOCUS_LOSS_TRANSIENT:
        focusChangeStr = "AUDIOFOCUS_LOSS_TRANSIENT";
        break;
      case AudioManager.AUDIOFOCUS_LOSS_TRANSIENT_CAN_DUCK:
        focusChangeStr = "AUDIOFOCUS_LOSS_TRANSIENT_CAN_DUCK";
        break;
      case AudioManager.AUDIOFOCUS_NONE:
        focusChangeStr = "AUDIOFOCUS_NONE";
        break;
      default:
        focusChangeStr = "AUDIOFOCUS_UNKNOWN";
        break;
    }

    Log.d(TAG, "AUDIO FOCUS CHANGED: " + focusChange + " - " + focusChangeStr);
  }
}
